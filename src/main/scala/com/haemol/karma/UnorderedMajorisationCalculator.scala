package com.haemol.karma

class UnorderedMajorisationCalculator {

  /**
   * For given n probabilities p_i of events of type y >= x_i calculates probability
   * of n independent ys would majorise in some order these xs.
   */
  def calculate(majorisationProbabilities: Seq[Double]): Double = {
    calculateRec(majorisationProbabilities.sorted)
  }

  private def calculateRec(majorisationProbabilities: Seq[Double]): Double = {
    majorisationProbabilities match {
      case Seq() => 0.0
      case Seq(p) => p
      case Seq(p1, p2, tail@_*) => p1 * calculateRec(p2 +: tail) + diagonalStep(Seq.empty, p1, p2, tail)
    }
  }

  /**
   * This handles different ordering of ys. If e.g. y_1 < x_1, but y_1 >= x_i for some i,
   * then we can omit such x_1 for later and require some other y_j to be greater in recursive step.
   *
   * The current probability p_current is the one for which we assume y_1 >= x_current.
   * All other values take part in recursive calculations.
   *
   * Diagonal step continues until all assignments are exhausted.
   */
  private def diagonalStep(omitted: Seq[Double], previous: Double, current: Double, tail: Seq[Double]): Double = {
    val intervalProbability = current - previous
    val currentExpression = intervalProbability * calculateRec((omitted :+ previous) ++ tail)
    tail match {
      case Seq() => currentExpression
      case Seq(p, subtail@_*) => currentExpression + diagonalStep(omitted :+ previous, current, p, subtail)
    }
  }

}
