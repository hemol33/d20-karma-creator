# D20 Karma Creator

A simple web creator for d20 abilities calculating karma - 
a probabilistic measure of luck of given attribute rolls.

Courtesy of Gitlab Pages feature, a working page is available at: https://hemol33.gitlab.io/d20-karma-creator/
