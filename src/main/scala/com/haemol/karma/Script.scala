package com.haemol.karma

import org.scalajs.dom
import org.scalajs.dom.document

object Script {

  private val KarmaId = "karma_display"

  def main(args: Array[String]): Unit = synchronized {
    val calculator = new KarmaCalculator
    val karmaNode = document.getElementById("karma").asInstanceOf[dom.Node]

    def recalculate(event: dom.Event): Unit = synchronized {
       val newValue = calculator.calculate(getInputs.map(_.value.toInt))
      deleteDisplay()
      putDisplay(newValue)
    }

    def deleteDisplay(): Unit = {
      karmaNode.removeChild(document.getElementById(KarmaId).asInstanceOf[dom.Node])
    }

    def putDisplay(value: Double): Unit = {
      val parNode = document.createElement("p")
      val percentageValue = 100*value
      parNode.id = KarmaId
      parNode.textContent = f"Karma used: $percentageValue%.3f"
      karmaNode.appendChild(parNode)
    }

    val inputs = getInputs
    inputs.foreach(_.addEventListener("change", recalculate))
    val attrs = inputs
      .map(_.value.toInt)
    putDisplay(calculator.calculate(attrs))
  }

  def getInputs: Seq[dom.html.Input] = {
    Seq("str_in", "dex_in", "con_in", "int_in", "wis_in", "cha_in")
      .map(document.getElementById(_).asInstanceOf[dom.html.Input])
  }

}
