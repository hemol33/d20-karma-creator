package com.haemol.karma

import com.haemol.karma.AttributeProbabilities.majorisationProbabilities

class KarmaCalculator {

  def calculate(rolls: Seq[Int]): Double = {
    val probabilitiesCalculator = new UnorderedMajorisationCalculator

    val karmaMax = if (rolls.size == 6) {
      KarmaCalculator.max6
    } else {
      1 - probabilitiesCalculator.calculate((1 to rolls.size).map(_ => majorisationProbabilities(18)))
    }

    (1 - probabilitiesCalculator.calculate(rolls.map(majorisationProbabilities))) / karmaMax
  }
}

object KarmaCalculator {
  val max6: Double = "0.9999999999818997".toDouble
}